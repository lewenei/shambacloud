<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Details</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
    <!-- Icon -->
    <link rel="stylesheet" type="text/css" href="/assets/fonts/line-icons.css">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="/assets/css/slicknav.css">

    <!-- Nivo Lightbox -->
    <link rel="stylesheet" type="text/css" href="/assets/css/nivo-lightbox.css">
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="/assets/css/animate.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" type="text/css" href="/assets/css/owl.carousel.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="/assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="/assets/css/responsive.css">

  </head>
  <body>

      <!-- Header Area wrapper Starts -->
      @include('front.partials.newheader')
      <!-- Header Area wrapper End -->

    <!-- Page Header Start -->
    <div class="page-header" style="background: url(assets/img/banner1.jpg);">
      <div class="container">
        <div class="row">         
          <div class="col-md-12">
            <div class="breadcrumb-wrapper">
              <h2 class="product-title">Ad Details</h2>
              <ol class="breadcrumb">
                <li><a href="#">Home /</a></li>
                <li class="current">Ad Details</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page Header End -->  

    <!-- Ads Details Start -->
    <div class="section-padding">
      <div class="container">
        <!-- Product Info Start -->
        <div class="product-info row">
          <div class="col-lg-7 col-md-12 col-xs-12">
            <div class="details-box ads-details-wrapper">
              <div id="owl-demo" class="owl-carousel owl-theme">

               @foreach (json_decode($addpost->images) as $image)
                <div class="item" @if ($loop->first) active @endif>
                  <div class="product-img">
                    <img class="img-fluid" src="/storage/post_photos/{{ $image }}" alt="">
                  </div>
                  <span class="price">Ksh {{$addpost->price}}</span>
                </div>
                @endforeach
              </div>
            </div>
          </div>
          <div class="col-lg-5 col-md-12 col-xs-12">
            <div class="details-box">
              <div class="ads-details-info">
                <h2>{{$addpost->title}}</h2>
                <p class="mb-2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum saepe suscipit debitis neque, laborum! Magni ducimus suscipit modi.</p>
                <div class="details-meta">
                  <span><a href="#"><i class="lni-alarm-clock"></i> 7 Jan, 10:10 pm</a></span>
                  <span><a href="#"><i class="lni-map-marker"></i>  {{$addpost->division}}</a></span>
                  <span><a href="#"><i class="lni-eye"></i> 299 View</a></span>
                </div>
                <h4 class="title-small mb-3">Specification:</h4>
                <ul class="list-specification">
                  <li> <i class="lni-check-mark-circle"></i> 256GB PCIe flash storage</li>
                  <li> <i class="lni-check-mark-circle"></i> 2.7 GHz dual-core Intel Core i5</li>
                  <li> <i class="lni-check-mark-circle"></i> Turbo Boost up to 3.1GHz</li>
                  <li> <i class="lni-check-mark-circle"></i> Intel Iris Graphics 6100</li>
                  <li> <i class="lni-check-mark-circle"></i> 8GB memory</li>
                  <li> <i class="lni-check-mark-circle"></i> 10 hour battery life</li>
                  <li> <i class="lni-check-mark-circle"></i> 13.3" Retina Display</li>
                  <li> <i class="lni-check-mark-circle"></i> 1 Year international warranty</li>
                </ul>
              </div>
              <ul class="advertisement mb-4">
                <li>
                <p><strong>Type:</strong> <a href="#">{{$addpost->category->name}}</a> <a href="#">For sale</a></p>
                </li>
                <li>
                <p><strong>Condition:</strong> {{$addpost->condition}}</p>
                </li>
                <li>
                <p><strong>Brand:</strong> <a href="#">Apple</a></p>
                </li>
              </ul>
              <div class="ads-btn mb-4">
                <a href="#" class="btn btn-common btn-reply"><i class="lni-envelope"></i> {{$addpost->user->email}}</a>
                <a href="#" class="btn btn-common"><i class="lni-phone-handset"></i>{{$addpost->user->phone_number}}</a>
              </div>
              <div class="share">
                <span>Share: </span>
                <div class="social-link">  
                  <a class="facebook" href="#"><i class="lni-facebook-filled"></i></a>
                  <a class="twitter" href="#"><i class="lni-twitter-filled"></i></a>
                  <a class="linkedin" href="#"><i class="lni-linkedin-fill"></i></a>
                  <a class="google" href="#"><i class="lni-google-plus"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Product Info End -->

        <!-- Product Description Start -->
        <div class="description-info">
          <div class="row">
            <div class="col-lg-8 col-md-6 col-xs-12">
              <div class="description">
                <h4>Description</h4>
                <p>{{$addpost->description}}</p>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
              </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12">
              <div class="short-info">
                <h4>Short Info</h4>
                <ul>
                  <li><a href="{{ route('user_adds',$addpost->user->id) }}"><i class="lni-users"></i> More ads by <span>{{$addpost->user->name}}</span></a></li>
                  <li><a href="#"><i class="lni-printer"></i> Print this ad</a></li>
                  <li><a href="#"><i class="lni-reply"></i> Send to a friend</a></li>
                  <li><a href="#"><i class="lni-warning"></i> Report this ad</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <!-- Product Description End -->
      </div>    
    </div>
    <!-- Ads Details End -->

    <!-- Featured Listings Start -->
    <section class="featured-lis section-padding" >
      <div class="container">
        <h3 class="section-title">Featured Products</h3>
        <div class="row">
          <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="product-item">
              <div class="carousel-thumb">
                <img class="img-fluid" src="assets/img/product/img1.jpg" alt=""> 
                <div class="overlay">
                </div> 
                <span class="price">89.00 $</span>
              </div>    
              <div class="product-content">
                <h3 class="product-title"><a href="ads-details.html">Laptop</a></h3>
                <a href="#"><i class="lni-bookmark"></i> New York</a>
                <a href="#"><i class="lni-map-marker"></i> California</a>
                <div class="icon">
                  <i class="lni-heart"></i>
                </div> 
                <div class="card-text">
                  <div class="meta">
                    <div class="float-left">
                      <span class="icon-wrap">
                        <i class="lni-star-filled"></i>
                        <i class="lni-star-filled"></i>
                        <i class="lni-star"></i>
                        <i class="lni-star"></i>
                      </span>
                      <span class="count-review">
                        <span>1</span> Reviews
                      </span>
                    </div>
                    <div class="float-right">
                      <span class="btn-product bg-red"><a href="#">{{$addpost->condition}}</a></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="product-item">
              <div class="carousel-thumb">
                <img class="img-fluid" src="assets/img/product/img2.jpg" alt=""> 
                <div class="overlay">
                </div> 
                <span class="price">89.00 $</span>
              </div>    
              <div class="product-content">
                <h3 class="product-title"><a href="ads-details.html">Headphones</a></h3>
                <a href="#"><i class="lni-bookmark"></i> New York</a>
                <a href="#"><i class="lni-map-marker"></i> California</a>
                <div class="icon">
                  <i class="lni-heart"></i>
                </div> 
                <div class="card-text">
                  <div class="meta">
                    <div class="float-left">
                      <span class="icon-wrap">
                        <i class="lni-star-filled"></i>
                        <i class="lni-star-filled"></i>
                        <i class="lni-star-filled"></i>
                        <i class="lni-star"></i>
                      </span>
                      <span class="count-review">
                        <span>1</span> Reviews
                      </span>
                    </div>
                    <div class="float-right">
                      <span class="btn-product bg-yellow"><a href="#">Sale</a></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="product-item">
              <div class="carousel-thumb">
                <img class="img-fluid" src="assets/img/product/img3.jpg" alt=""> 
                <div class="overlay">
                </div> 
                <span class="price">49.00 $</span>
              </div>    
              <div class="product-content">
                <h3 class="product-title"><a href="ads-details.html">Furniture</a></h3>
                <a href="#"><i class="lni-bookmark"></i> New York</a>
                <a href="#"><i class="lni-map-marker"></i> California</a>
                <div class="icon">
                  <i class="lni-heart"></i>
                </div> 
                <div class="card-text">
                  <div class="meta">
                    <div class="float-left">
                      <span class="icon-wrap">
                        <i class="lni-star-filled"></i>
                        <i class="lni-star-filled"></i>
                        <i class="lni-star-filled"></i>
                        <i class="lni-star-filled"></i>
                      </span>
                      <span class="count-review">
                        <span>1</span> Reviews
                      </span>
                    </div>
                    <div class="float-right">
                      <span class="btn-product bg-red"><a href="#">New</a></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Featured Listings End -->

    <!-- Footer Section Start -->
    <footer>
      <!-- Footer Area Start -->
      <section class="footer-Content">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">About Us</h3>
                <div class="textwidget">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, consectetur eget facilisis sed, auctor ut purus.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Useful Links</h3>
                <ul class="menu">
                  <li><a href="#">How to Sell Faster</a></li>
                  <li><a href="#">Membership Details</a></li>
                  <li><a href="#">Why Choose</a></li>
                  <li><a href="#">Job Opennings</a></li>
                  <li><a href="#">Using This Platform</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Help & Support</h3>
                <ul class="menu">
                  <li><a href="#">Live Chat</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Purchase Protection</a></li>
                  <li><a href="#">Support</a></li>
                  <li><a href="#">Contact us</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Contact Information</h3>
                <ul class="contact-footer">
                  <li>
                    <strong>Address :</strong><span>1900 Pico Blvd, New York</span>
                  </li>
                  <li>
                    <strong>Phone :</strong><span>+48 123 456 789</span>
                  </li>
                  <li>
                    <strong>E-mail :</strong><span><a href="#">info@example.com</a></span>
                  </li>
                </ul>  
                <ul class="footer-social">
                  <li><a class="facebook" href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a class="twitter" href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a class="linkedin" href="#"><i class="lni-linkedin-fill"></i></a></li>
                  <li><a class="google-plus" href="#"><i class="lni-google-plus"></i></a></li>
                </ul>          
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Footer area End -->
      
      <!-- Copyright Start  -->
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="site-info float-left">
                <p>All copyrights reserved &copy; 2018 - Designed by <a href="https://uideck.com" rel="nofollow">UIdeck</a></p>
              </div>              
              <div class="float-right">  
                <ul class="bottom-card">
                  <li>
                      <a href="#"><img src="assets/img/footer/card1.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card2.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card3.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card4.jpg" alt="card"></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Copyright End -->

   
    </footer>
    <!-- Footer Section End --> 

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
      <i class="lni-chevron-up"></i>
    </a>

    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/assets/js/jquery-min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/wow.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>
    <script src="/assets/js/nivo-lightbox.js"></script>
    <script src="/assets/js/jquery.slicknav.js"></script>
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/form-validator.min.js"></script>
    <script src="/assets/js/contact-form-script.min.js"></script>
    <script src="/assets/js/summernote.js"></script>
      
  </body>
</html>