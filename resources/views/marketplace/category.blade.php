<!DOCTYPE html>
<html lang="en">
  @include('marketplace.includes.head')
  <body>

    <!-- Header Area wrapper Starts -->
    @include('front.partials.newheader')
    <!-- Header Area wrapper End -->

    <!-- Main container Start -->  
    <div class="main-container section-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-12 col-xs-12 page-sidebar">
            <aside>
              <!-- Searcg Widget -->
              <div class="widget widget_search">
                <form role="search" id="search-form">
                  <input type="search" class="form-control" autocomplete="off" name="s" placeholder="Search..." id="search-input" value="">
                  <button type="submit" id="search-submit" class="search-btn"><i class="lni-search"></i></button>
                </form>
              </div>
              <!-- Categories Widget -->
              <div class="widget categories">
                <h4 class="widget-title">All Categories</h4>
                <ul class="categories-list">
                    @foreach ($all_categories as $category)
                  <li>
                    
                    <a href="{{ route('category_adds',$category->slug) }}">
                        <img class="img-fluid" style="width:25px;height:25px;" src="/storage/category_photos/{{$category->image}}" alt="">
                      {{$category->name}} <span class="category-counter">({{$category->addposts->count()}})</span>
                    </a>
                  </li>
                  @endforeach
                </ul>
              </div>
              <div class="widget">
                <h4 class="widget-title">Advertisement</h4>
                <div class="add-box">
                  <img class="img-fluid" src="assets/img/img1.jpg" alt="">
                </div>
              </div>
            </aside>
          </div>
          <div class="col-lg-9 col-md-12 col-xs-12 page-content">
            <!-- Product filter Start -->
            <div class="product-filter">
              <div class="short-name">
                <span>Showing (1 - 12 products of 7371 products)</span>
              </div>
              <div class="Show-item">
                <span>Show Items</span>
                <form class="woocommerce-ordering" method="post">
                  <label>
                    <select name="order" class="orderby">
                      <option selected="selected" value="menu-order">49 items</option>
                      <option value="popularity">popularity</option>
                      <option value="popularity">Average ration</option>
                      <option value="popularity">newness</option>
                      <option value="popularity">price</option>
                    </select>
                  </label>
                </form>
              </div>
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#grid-view"><i class="lni-grid"></i></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#list-view"><i class="lni-list"></i></a>
                </li>
              </ul>
            </div>
            <!-- Product filter End -->

            <!-- Adds wrapper Start -->
            <div class="adds-wrapper">
              <div class="tab-content">
                <div id="grid-view" class="tab-pane fade">
                  <div class="row">
                    @foreach ($all_posts as $addpost)
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <div class="featured-box">
                            <figure>
                              <div class="icon">
                                <i class="lni-heart"></i>
                              </div>
                              <a href="{{ route('user.addpost.show',$addpost->id) }}">
                                  @foreach (json_decode($addpost->images) as $image)
                                  @if ($loop->first)
                                  <img class="img-fluid" src="/storage/post_photos/{{$image}}" alt=""></a>
                                  @endif
                              @endforeach
                            </figure>
                            <div class="feature-content">
                              <div class="tg-product">
                                <a href="{{ route('category_adds',$addpost->category->slug) }}">{{$addpost->category->name}}</a>
                              </div>
                              <h4><a href="ads-details.html">{{$addpost->brand_name}}</a></h4>
                              <span>Last Updated: 5 hours ago</span>
                              <ul class="address">
                                <li>
                                  <a href="javascript:void(0)"><i class="lni-map-marker"></i> {{$addpost->area}}</a>
                                </li>
                                <li>
                                  <a href="#"><i class="lni-alarm-clock"></i> {{$addpost->created_at->diffForHumans()}}</a>
                                </li>
                                <li>
                                  <a href="{{ route('user_adds',$addpost->user->id) }}"><i class="lni-user"></i> {{$addpost->user->name}}</a>
                                </li>
                                <li>
                                  <a href="#"><i class="lni-package"></i> {{$addpost->condition}}</a>
                                </li>
                              </ul>
                              <div class="btn-list">
                                <a class="btn-price" href="#">Ksh {{$addpost->price}}</a>
                                <a class="btn btn-common" href="{{ route('user.addpost.show',$addpost->id) }}">
                                  <i class="lni-list"></i>
                                  View Details
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                    @endforeach
                  </div>
                </div>
                <div id="list-view" class="tab-pane fade active show">
                  <div class="row">
                    @foreach ($all_posts as $addpost)
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="featured-box">
                            <figure>
                              <div class="icon">
                                <i class="lni-heart"></i>
                              </div>
                              <a href="{{ route('user.addpost.show',$addpost->id) }}">
                                  @foreach (json_decode($addpost->images) as $image)
                                  @if ($loop->first)
                                  <img class="img-fluid" src="/storage/post_photos/{{$image}}" alt=""></a>
                                  @endif
                              @endforeach
                            </figure>
                            <div class="feature-content">
                              <div class="tg-product">
                                <a href="{{ route('category_adds',$addpost->category->slug) }}">{{$addpost->category->name}}</a>
                              </div>
                              <h4><a href="ads-details.html">{{$addpost->brand_name}}</a></h4>
                              <span>Last Updated:{{$addpost->created_at->diffForHumans()}}</span>
                              <ul class="address">
                                <li>
                                  <a href="javascript:void(0)"><i class="lni-map-marker"></i> {{$addpost->area}}</a>
                                </li>
                                <li>
                                  <a href="#"><i class="lni-alarm-clock"></i> {{$addpost->created_at->diffForHumans()}}</a>
                                </li>
                                <li>
                                  <a href="{{ route('user_adds',$addpost->user->id) }}"><i class="lni-user"></i> {{$addpost->user->name}}</a>
                                </li>
                                <li>
                                  <a href="#"><i class="lni-package"></i> {{$addpost->condition}}</a>
                                </li>
                              </ul>
                              <div class="btn-list">
                                <a class="btn-price" href="#">Ksh {{$addpost->price}}</a>
                                <a class="btn btn-common" href="{{ route('user.addpost.show',$addpost->id) }}">
                                  <i class="lni-list"></i>
                                  View Details
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                    
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
            <!-- Adds wrapper End -->
      
            <!-- Start Pagination -->
            <div class="pagination-bar">
               <nav>
                <ul class="pagination">
                  <li class="page-item"><a class="page-link active" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">Next</a></li>
                </ul>
              </nav>
            </div>
            <!-- End Pagination -->
            
          </div>
        </div>
      </div>
    </div>
    <!-- Main container End -->  

    <!-- Footer Section Start -->
    <footer>
      <!-- Footer Area Start -->
      <section class="footer-Content">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">About Us</h3>
                <div class="textwidget">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, consectetur eget facilisis sed, auctor ut purus.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Useful Links</h3>
                <ul class="menu">
                  <li><a href="#">How to Sell Faster</a></li>
                  <li><a href="#">Membership Details</a></li>
                  <li><a href="#">Why Choose</a></li>
                  <li><a href="#">Job Opennings</a></li>
                  <li><a href="#">Using This Platform</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Help & Support</h3>
                <ul class="menu">
                  <li><a href="#">Live Chat</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Purchase Protection</a></li>
                  <li><a href="#">Support</a></li>
                  <li><a href="#">Contact us</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Contact Information</h3>
                <ul class="contact-footer">
                  <li>
                    <strong>Address :</strong><span>1900 Pico Blvd, New York</span>
                  </li>
                  <li>
                    <strong>Phone :</strong><span>+48 123 456 789</span>
                  </li>
                  <li>
                    <strong>E-mail :</strong><span><a href="#">info@example.com</a></span>
                  </li>
                </ul>  
                <ul class="footer-social">
                  <li><a class="facebook" href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a class="twitter" href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a class="linkedin" href="#"><i class="lni-linkedin-fill"></i></a></li>
                  <li><a class="google-plus" href="#"><i class="lni-google-plus"></i></a></li>
                </ul>          
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Footer area End -->
      
      <!-- Copyright Start  -->
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="site-info float-left">
                <p>All copyrights reserved &copy; 2018 - Designed by <a href="https://uideck.com" rel="nofollow">UIdeck</a></p>
              </div>              
              <div class="float-right">  
                <ul class="bottom-card">
                  <li>
                      <a href="#"><img src="assets/img/footer/card1.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card2.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card3.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card4.jpg" alt="card"></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Copyright End -->

    </footer>
    <!-- Footer Section End --> 

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
      <i class="lni-chevron-up"></i>
    </a>

    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/assets/js/jquery-min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/wow.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>
    <script src="/assets/js/nivo-lightbox.js"></script>
    <script src="/assets/js/jquery.slicknav.js"></script>
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/form-validator.min.js"></script>
    <script src="/assets/js/contact-form-script.min.js"></script>
    <script src="/assets/js/summernote.js"></script>
      
  </body>
</html>