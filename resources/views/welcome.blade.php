<!DOCTYPE html>
<html lang="en">
  @include('marketplace.includes.head')
  <body>

    <!-- Header Area wrapper Starts -->
  @include('front.partials.newheader')
    <!-- Header Area wrapper End -->

    <!-- Trending Categories Section Start -->
    <section class="trending-cat section-padding">
      <div class="container">
        <h1 class="section-title">Product Categories</h1>
        <div class="row">
            @foreach ($all_categories as $category)
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <a href="{{ route('category_adds',$category->slug) }}">
              <div class="box">
                <div class="icon">
                  <img class="img-fluid" src="/storage/category_photos/{{$category->image}}" alt="">
                </div>
                <h4>{{$category->name}}</h4>
                <strong>({{$category->addposts->count()}}) Ads</strong>
              </div>
            </a>
          </div>
          @endforeach
        </div>
      </div>
    </section>
    <!-- Trending Categories Section End -->

    <!-- Featured Section Start -->
    <section class="featured section-padding">
      <div class="container">
        <h1 class="section-title">Latest Products</h1>
        
        <div class="row">
            @foreach ($all_posts as $addpost)
          <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
            <div class="featured-box">
              <figure>
                <div class="icon">
                  <i class="lni-heart"></i>
                </div>
                <a href="{{ route('user.addpost.show',$addpost->id) }}">
                    @foreach (json_decode($addpost->images) as $image)
                    @if ($loop->first)
                    <img class="img-fluid" style="width:300px;height:200px;" src="/storage/post_photos/{{$image}}" alt=""></a>
                    @endif
                @endforeach
              </figure>
              <div class="feature-content">
                <div class="tg-product">
                  <a href="{{ route('category_adds',$addpost->category->slug) }}">{{$addpost->category->name}}</a>
                </div>
                <h4>{{$addpost->brand_name}}</h4>
                <span>Last Updated: {{$addpost->updated_at->diffForHumans()}}</span>
                <ul class="address">
                  <li>
                    <a href="javascript:void(0)"><i class="lni-map-marker"></i> {{$addpost->area}}</a>
                  </li>
                  <li>
                    <a href="#"><i class="lni-alarm-clock"></i> {{$addpost->created_at->diffForHumans()}}</a>
                  </li>
                  <li>
                    <a href="{{ route('user_adds',$addpost->user->id) }}"><i class="lni-user"></i> {{$addpost->user->name}}</a>
                  </li>
                  <li>
                    <a href="#"><i class="lni-package"></i> {{$addpost->condition}}</a>
                  </li>
                </ul>
                <div class="btn-list">
                  <a class="btn-price" href="#">Ksh {{$addpost->price}}</a>
                  <a class="btn btn-common" href="{{ route('user.addpost.show',$addpost->id) }}">
                    <i class="lni-list"></i>
                    View Details
                  </a>
                </div>
              </div>
            </div>
          </div>

          @endforeach
        </div>
    <!-- pagination  -->
        {{$all_posts->links()}}
    <!-- pagination  -->
      </div>
    </section>
    <!-- Featured Section End -->

    <!-- Featured Listings Start -->
    <section class="featured-lis section-padding" >
      <div class="container">
        <div class="row">
          @foreach ($all_posts as $addpost)
          <div class="col-md-12 wow fadeIn" data-wow-delay="0.5s">
            <h3 class="section-title">Featured Products</h3>
            <div id="new-products" class="owl-carousel">
              <div class="item">
                <div class="product-item">
                  <div class="carousel-thumb">
                    @foreach (json_decode($addpost->images) as $image)
                    @if ($loop->first)
                    <img class="img-fluid" style="width:300px;height:200px;" src="/storage/post_photos/{{$image}}" alt=""></a>
                    @endif
                @endforeach
                    <div class="overlay">
                    </div> 
                    <span class="price">Ksh {{$addpost->price}}</span>
                  </div>    
                  <div class="product-content">
                    <h3 class="product-title"><a href="{{ route('user.addpost.show',$addpost->id) }}">{{$addpost->brand_name}}</a></h3>
                    <a href="#"><i class="lni-bookmark"></i> {{$addpost->area}}</a>
                    <div class="icon">
                      <i class="lni-heart"></i>
                    </div> 
                    <div class="card-text">
                      <div class="meta">
                        <div class="float-left">
                          <span class="icon-wrap">
                            <i class="lni-star-filled"></i>
                            <i class="lni-star-filled"></i>
                            <i class="lni-star"></i>
                            <i class="lni-star"></i>
                          </span>
                          <span class="count-review">
                            <span>1</span> Reviews
                          </span>
                        </div>
                        <div class="float-right">
                          <span class="btn-product bg-red"><a href="#">New</a></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 
          @endforeach
        </div>
      </div>
    </section>
    <!-- Featured Listings End -->





    <!-- Special Ooffer Section Start -->
<!--    <section class="special-offer section-padding">
      <div class="container">
        <h1 class="section-title">Daily Deals</h1>
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <a href="ads-details.html"><div class="special-product">
              <img src="assets/img/gallery/img-1.jpg" alt="">
              <div class="product-text">
                <h3>Special Offer</h3>
                <div class="offer-details">
                  <h5 class="price">$ 1400</h5>
                  <h4>Buy IphoneX</h4>
                  <p>with special gift</p>
                </div>
                <div class="icon-footer">
                  <a href="#"><i class="icon-arrow-right-circle"></i></a>
                </div>
              </div>
            </div></a>
          </div>
          <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <a href="ads-details.html"><div class="special-product">
              <img src="assets/img/gallery/img-2.jpg" alt="">
              <div class="product-text">
                <h3>Special Offer</h3>
                <div class="offer-details">
                  <h5 class="price">$ 850</h5>
                  <h4>Buy Galaxy Note 8</h4>
                  <p>with special gift</p>
                </div>
                <div class="icon-footer">
                  <a href="#"><i class="icon-arrow-right-circle"></i></a>
                </div>
              </div>
            </div></a>
          </div>
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
            <div class="row">
              <div class="col-lg-12 col-md-6 col-sm-12">
                <a href="ads-details.html"><div class="special-product mb-30">
                  <img class="img-fluid" src="assets/img/gallery/img-3.jpg" alt="">
                  <div class="product-text">
                    <p class="text">Colorful Outdoor <br> Mattresses That Connect to Each Other</p>
                    <div class="icon-footer">
                      <h5 class="price">$ 76</h5>
                    </div>
                  </div>
                </div></a>
              </div>
              <div class="col-lg-12 col-md-6 col-sm-12">
                <a href="ads-details.html"><div class="special-product">
                  <img class="img-fluid" src="assets/img/gallery/img-5.jpg" alt="">
                  <div class="product-text">
                    <p class="text">Handmade Hardwood & <br> Rope Toys from Monroe Workshop</p>
                    <div class="icon-footer">
                      <h5 class="price">$ 50</h5>
                    </div>
                  </div>
                </div></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- Special Ooffer Section End -->

    <!-- Testimonial Section Start -->
    <section class="testimonial section-padding">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="testimonials" class="owl-carousel">
              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    <img src="assets/img/testimonial/img1.png" alt="">
                  </div>
                  <div class="content">
                    <h2><a href="#">John Doe</a></h2>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo quidem, excepturi facere magnam illum, at accusantium doloremque odio.</p>
                    <h3>Developer at of <a href="#">xyz company</a></h3>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    <img src="assets/img/testimonial/img2.png" alt="">
                  </div>
                  <div class="content">
                    <h2><a href="#">Jessica</a></h2>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo quidem, excepturi facere magnam illum, at accusantium doloremque odio.</p>
                    <h3>Developer at of <a href="#">xyz company</a></h3>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    <img src="assets/img/testimonial/img3.png" alt="">
                  </div>
                  <div class="content">
                    <h2><a href="#">Johnny Zeigler</a></h2>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo quidem, excepturi facere magnam illum, at accusantium doloremque odio.</p>
                    <h3>Developer at of <a href="#">xyz company</a></h3>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    <img src="assets/img/testimonial/img1.png" alt="">
                  </div>
                  <div class="content">
                    <h2><a href="#">John Doe</a></h2>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo quidem, excepturi facere magnam illum, at accusantium doloremque odio.</p>
                    <h3>Developer at of <a href="#">xyz company</a></h3>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimonial-item">
                  <div class="img-thumb">
                    <img src="assets/img/testimonial/img2.png" alt="">
                  </div>
                  <div class="content">
                    <h2><a href="#">Jessica</a></h2>
                    <p class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo quidem, excepturi facere magnam illum, at accusantium doloremque odio.</p>
                    <h3>Developer at of <a href="#">xyz company</a></h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Testimonial Section End -->

    <!-- Subscribe Section Start -->
    <section class="subscribes section-padding">
      <div class="container">
        <div class="row wrapper-sub">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <p>Join our 10,000+ subscribers and get access to the latest deals, announcements and resources!</p>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <form>
              <div class="subscribe">
                <input class="form-control" name="email" placeholder="Your email here" required="" type="email">
                <button class="btn btn-common" type="submit">Subscribe</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- Subscribe Section End -->

    <!-- Footer Section Start -->
    <footer>
      <!-- Footer Area Start -->
      <section class="footer-Content">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">About Us</h3>
                <div class="textwidget">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, consectetur eget facilisis sed, auctor ut purus.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Useful Links</h3>
                <ul class="menu">
                  <li><a href="#">How to Sell Faster</a></li>
                  <li><a href="#">Membership Details</a></li>
                  <li><a href="#">Why Choose</a></li>
                  <li><a href="#">Job Opennings</a></li>
                  <li><a href="#">Using This Platform</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Help & Support</h3>
                <ul class="menu">
                  <li><a href="#">Live Chat</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Purchase Protection</a></li>
                  <li><a href="#">Support</a></li>
                  <li><a href="#">Contact us</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Contact Information</h3>
                <ul class="contact-footer">
                  <li>
                    <strong>Address :</strong><span>1900 Pico Blvd, New York</span>
                  </li>
                  <li>
                    <strong>Phone :</strong><span>+48 123 456 789</span>
                  </li>
                  <li>
                    <strong>E-mail :</strong><span><a href="#">info@example.com</a></span>
                  </li>
                </ul>  
                <ul class="footer-social">
                  <li><a class="facebook" href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a class="twitter" href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a class="linkedin" href="#"><i class="lni-linkedin-fill"></i></a></li>
                  <li><a class="google-plus" href="#"><i class="lni-google-plus"></i></a></li>
                </ul>          
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Footer area End -->
      
      <!-- Copyright Start  -->
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="site-info float-left">
                <p>All copyrights reserved &copy; 2018 - Designed by <a href="https://uideck.com" rel="nofollow">UIdeck</a></p>
              </div>              
              <div class="float-right">  
                <ul class="bottom-card">
                  <li>
                      <a href="#"><img src="assets/img/footer/card1.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card2.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card3.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card4.jpg" alt="card"></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Copyright End -->

    </footer>
    <!-- Footer Section End --> 

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
    	<i class="lni-chevron-up"></i>
    </a>

    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/assets/js/jquery-min.js"></script>
    <script src="/assets/js/popper.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.counterup.min.js"></script>
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/wow.js"></script>
    <script src="/assets/js/owl.carousel.min.js"></script>
    <script src="/assets/js/nivo-lightbox.js"></script>
    <script src="/assets/js/jquery.slicknav.js"></script>
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/form-validator.min.js"></script>
    <script src="/assets/js/contact-form-script.min.js"></script>
    <script src="/assets/js/summernote.js"></script>
      
  </body>
</html>
