    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
        <!-- Navbar Start -->
        <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                <span class="lni-menu"></span>
                <span class="lni-menu"></span>
                <span class="lni-menu"></span>
              </button>
              <a href="/" class="navbar-brand"><img src="assets/img/logo.png" alt=""></a>
            </div>
            <div class="collapse navbar-collapse" id="main-navbar">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="{{ route('home') }}">
                    Home
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/marketplace/category/ads">
                    Categories
                  </a>
                </li>
               <!-- <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Listings
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="adlistinggrid.html">Ad Grid</a>
                    <a class="dropdown-item" href="adlistinglist.html">Ad Listing</a>
                    <a class="dropdown-item" href="ads-details.html">Listing Detail</a>
                  </div>
                </li> -->
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Pages 
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="about.html">About Us</a>
                    <a class="dropdown-item" href="services.html">Services</a>
                    <a class="dropdown-item" href="ads-details.html">Ads Details</a>
                    <a class="dropdown-item" href="post-ads.html">Ads Post</a>
                    <a class="dropdown-item" href="pricing.html">Packages</a>
                    <a class="dropdown-item" href="testimonial.html">Testimonial</a>
                    <a class="dropdown-item" href="faq.html">FAQ</a>
                    <a class="dropdown-item" href="404.html">404</a>
                  </div>
                </li>
               <!-- <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Blog 
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="blog.html">Blog - Right Sidebar</a>
                    <a class="dropdown-item" href="blog-left-sidebar.html">Blog - Left Sidebar</a>
                    <a class="dropdown-item" href="blog-grid-full-width.html"> Blog full width </a>
                    <a class="dropdown-item" href="single-post.html">Blog Details</a>
                  </div>
                </li> -->
                <li class="nav-item">
                  <a class="nav-link" href="contact.html">
                    Contact
                  </a>
                </li>
              </ul>
              <ul class="sign-in">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="lni-user"></i> My Account</a>
                  <div class="dropdown-menu">
                    @guest
                    <a class="dropdown-item" href="{{ route('login') }}"><i class="lni-lock"></i> Log In</a>
                    @if (Route::has('register'))
                    <a class="dropdown-item" href="{{ route('register') }}"><i class="lni-user"></i> Signup</a>
                    @endif
                    @else
                    <a class="dropdown-item" href="forgot-password.html"><i class="lni-reload"></i> Forgot Password</a>
                    <a class="dropdown-item" href="account-close.html"><i class="lni-close"></i>Account close</a>
                  </div>
                </li>
              </ul>
              <a class="tg-btn" href="{{ route('user.addpost.create') }}">
                <i class="lni-pencil-alt"></i> Post An Ad
              </a>
              <a class="tg-btn" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        Logout </a>
                    
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    @endif
            </div>
          </div>
  
          <!-- Mobile Menu Start -->
          <ul class="mobile-menu">
            <li>
              <a class="active" href="{{ route('home') }}">Home</a>
            </li>
            <li>
              <a href="category.html">Categories</a>
            </li>
           <!-- <li>
              <a href="#">
              Listings
              </a>
              <ul class="dropdown">
                <li><a href="adlistinggrid.html">Ad Grid</a></li>
                <li><a href="adlistinglist.html">Ad Listing</a></li>
                <li><a href="ads-details.html">Listing Detail</a></li>
              </ul>
            </li> -->
           <!-- <li>
              <a href="#">Pages</a>
              <ul class="dropdown">
                <li><a href="about.html">About Us</a></li>
                <li><a href="services.html">Services</a></li>
                <li><a href="ads-details.html">Ads Details</a></li>
                <li><a href="post-ads.html">Ads Post</a></li>
                <li><a href="pricing.html">Packages</a></li>
                <li><a href="testimonial.html">Testimonial</a></li>
                <li><a href="faq.html">FAQ</a></li>
                <li><a href="404.html">404</a></li>
              </ul>
            </li>
            <li>
              <a href="#">Blog</a>
              <ul class="dropdown">
                <li><a href="blog.html">Blog - Right Sidebar</a></li>
                <li><a href="blog-left-sidebar.html">Blog - Left Sidebar</a></li>
                <li><a href="blog-grid-full-width.html"> Blog full width </a></li>
                <li><a href="single-post.html">Blog Details</a></li>
              </ul>
            </li> -->
            <li>
              <a href="contact.html">Contact Us</a>
            </li>
            <li>
              <a>My Account</a>
              <ul class="dropdown">
                  @guest
                <li><a href="{{ route('login') }}"><i class="lni-lock"></i> Log In</a></li>
                @if (Route::has('register'))
                <li><a href="{{ route('register') }}"><i class="lni-user"></i> Signup</a></li>
                @endif
              </ul>
              @else
                <li><a href="{{ route('user.addpost.create') }}"><i class="lni-reload"></i> Post Your Ad</a></li>
                <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><i class="lni-close"></i>Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
              </ul>
            </li>
            @endif
          </ul>
          <!-- Mobile Menu End -->
  
        </nav>
        <!-- Navbar End -->
  
        <!-- Hero Area Start -->
        <div id="hero-area">
          <div class="overlay"></div>
          <div class="container">
            <div class="row">
              <div class="col-md-12 col-lg-12 col-xs-12 text-center">
                <div class="contents">
                  <h1 class="head-title">Welcome to <span class="year">Shamba Cloud</span> Marketplace</h1>
                  <p>Buy And Sell Everything From Cereals to Livestock <br> Or Search For Equipment, Farm Jobs And More</p>
                  <div class="search-bar">
                    <fieldset>
                      <form class="search-form" action="{{ route('post_search') }}" method="GET">
                        <div class="form-group tg-inputwithicon">
                          <i class="lni-tag"></i>
                          <input type="text" name="search" class="form-control" placeholder="What are you looking for">
                        </div>
                        <div class="form-group tg-inputwithicon">
                          <i class="lni-map-marker"></i>
                          <div class="tg-select">
                            <select class="form-control" name="division">
                              <option value="">Select County</option>
                              @foreach ($all_division as $division)
                              <option value="{{$division}}">{{$division}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="form-group tg-inputwithicon">
                          <i class="lni-layers"></i>
                          <div class="tg-select">
                              <select class="form-control" name="category">
                                  <option value="" selected>Select Category</option>
                                  @foreach ($all_categories as $category)
                                      <option value="{{$category->id}}">{{$category->name}}</option>
                                  @endforeach
                              </select>
                          </div>
                        </div>
                        <button class="btn btn-common" value="search" type="submit"><i class="lni-search"></i></button>
                      </form>
                    </fieldset>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Hero Area End -->
  
      </header>
      <!-- Header Area wrapper End -->