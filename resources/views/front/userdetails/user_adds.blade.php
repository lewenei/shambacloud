<!DOCTYPE html>
<html lang="en">
    @include('marketplace.includes.head')
    <body>
  
      <!-- Header Area wrapper Starts -->
    @include('front.partials.newheader')
      <!-- Header Area wrapper End -->

    <!-- Page Header Start -->
    <div class="page-header" style="background: url(assets/img/banner1.jpg);">
      <div class="container">
        <div class="row">         
          <div class="col-md-12">
            <div class="breadcrumb-wrapper">
              <h2 class="product-title">My Ads</h2>
              <ol class="breadcrumb">
                <li><a href="#">Home /</a></li>
                <li class="current">My Ads</li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page Header End -->  

    <!-- Start Content -->
    <div id="content" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-4 col-lg-3 page-sidebar">
            <aside>
              <div class="sidebar-box">
                <div class="user">
                  <figure>
                    <a href="#"><img src="assets/img/author/img1.jpg" alt=""></a>
                  </figure>
                  <div class="usercontent">
                    <h3>User</h3>
                    <h4>Administrator</h4>
                  </div>
                </div>
                <nav class="navdashboard">
                  <ul>
                    <li>
                      <a href="dashboard.html">
                        <i class="lni-dashboard"></i>
                        <span>Dashboard</span>
                      </a>
                    </li>
                    <li>
                      <a href="account-profile-setting.html">
                        <i class="lni-cog"></i>
                        <span>Profile Settings</span>
                      </a>
                    </li>
                    <li>
                      <a class="active" href="account-myads.html">
                        <i class="lni-layers"></i>
                        <span>My Ads</span>
                      </a>
                    </li>
                    <li>
                      <a href="offermessages.html">
                        <i class="lni-envelope"></i>
                        <span>Offers/Messages</span>
                      </a>
                    </li>
                    <li>
                      <a href="payments.html">
                        <i class="lni-wallet"></i>
                        <span>Payments</span>
                      </a>
                    </li>
                    <li>
                      <a href="account-favourite-ads.html">
                        <i class="lni-heart"></i>
                        <span>My Favourites</span>
                      </a>
                    </li>
                    <li>
                      <a href="privacy-setting.html">
                        <i class="lni-star"></i>
                        <span>Privacy Settings</span>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="lni-enter"></i>
                        <span>Logout</span>
                      </a>
                    </li>
                  </ul>
                </nav>
              </div>
              <div class="widget">
                <h4 class="widget-title">Advertisement</h4>
                <div class="add-box">
                  <img class="img-fluid" src="assets/img/img1.jpg" alt="">
                </div>
              </div>
            </aside>
          </div>

          <div class="col-sm-12 col-md-8 col-lg-9">
            <div class="page-content">
              <div class="inner-box">
                <div class="dashboard-box">
                  <h2 class="dashbord-title">My Ads</h2>
                </div>
                <div class="dashboard-wrapper">
                 <nav class="nav-table">
                    <ul>
                      <li class="active"><a href="#">All Ads (42)</a></li>
                      <li><a href="#">Published (88)</a></li>
                      <li><a href="#">Featured (12)</a></li>
                      <li><a href="#">Sold (02)</a></li>
                      <li><a href="#">Active (42)</a></li>
                      <li><a href="#">Expired (01)</a></li>
                    </ul>
                  </nav>
                  <table class="table dashboardtable tablemyads">
                    <thead>
                      <tr>
                        <th>Photo</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Ad Status</th>
                        <th>Price</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr data-category="active">
                        @foreach ($userAdds as $add)
                        <td class="photo">
                            @foreach (json_decode($add->images) as $image)
                        @if ($loop->first)
                            <img class="img-fluid" src="/storage/post_photos/{{ $image }}" alt=""></td>
                            @endif
                            @endforeach
                        <td data-title="Title">
                          <h3>{{$add->title}}</h3>
                          <span>Ad ID: {{$add->id}}</span>
                        </td>
                        <td data-title="Category"><span class="adcategories">{{$add->category->name}}</span></td>
                        <td data-title="Ad Status"><span class="adstatus adstatusactive">active</span></td>
                        <td data-title="Price">
                          <h3>Ksh {{$add->price}}</h3>
                        </td>
                        <td data-title="Action">
                          <div class="btns-actions">
                            <a class="btn-action btn-view" title="View" href="#"><i class="lni-eye"></i></a>
                            <a class="btn-action btn-edit" title="Edit" href="{{ route('user.addpost.edit',$add->id) }}"><i class="lni-pencil"></i></a>
                            <form class="d-inline" action="{{ route('user.addpost.destroy',$add->id) }}" method="POST">
                                @csrf
                                {{method_field('DELETE')}}
                                <button
                                class="btn-action btn-delete" data-toggle="tooltip"
                                data-placement="top"
                                title="Delete"
                                type="submit"><i class="lni-trash"></i></button>

                            </form>

                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>  
      </div>      
    </div>
    <!-- End Content -->

    <!-- Footer Section Start -->
    <footer>
      <!-- Footer Area Start -->
      <section class="footer-Content">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">About Us</h3>
                <div class="textwidget">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque lobortis tincidunt est, et euismod purus suscipit quis. Etiam euismod ornare elementum. Sed ex est, consectetur eget facilisis sed, auctor ut purus.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Useful Links</h3>
                <ul class="menu">
                  <li><a href="#">How to Sell Faster</a></li>
                  <li><a href="#">Membership Details</a></li>
                  <li><a href="#">Why Choose</a></li>
                  <li><a href="#">Job Opennings</a></li>
                  <li><a href="#">Using This Platform</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Help & Support</h3>
                <ul class="menu">
                  <li><a href="#">Live Chat</a></li>
                  <li><a href="#">Privacy Policy</a></li>
                  <li><a href="#">Purchase Protection</a></li>
                  <li><a href="#">Support</a></li>
                  <li><a href="#">Contact us</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 col-mb-12">
              <div class="widget">
                <h3 class="block-title">Contact Information</h3>
                <ul class="contact-footer">
                  <li>
                    <strong>Address :</strong><span>1900 Pico Blvd, New York</span>
                  </li>
                  <li>
                    <strong>Phone :</strong><span>+48 123 456 789</span>
                  </li>
                  <li>
                    <strong>E-mail :</strong><span><a href="#">info@example.com</a></span>
                  </li>
                </ul>  
                <ul class="footer-social">
                  <li><a class="facebook" href="#"><i class="lni-facebook-filled"></i></a></li>
                  <li><a class="twitter" href="#"><i class="lni-twitter-filled"></i></a></li>
                  <li><a class="linkedin" href="#"><i class="lni-linkedin-fill"></i></a></li>
                  <li><a class="google-plus" href="#"><i class="lni-google-plus"></i></a></li>
                </ul>          
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- Footer area End -->
      
      <!-- Copyright Start  -->
      <div id="copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="site-info float-left">
                <p>All copyrights reserved &copy; 2018 - Designed by <a href="https://uideck.com" rel="nofollow">UIdeck</a></p>
              </div>              
              <div class="float-right">  
                <ul class="bottom-card">
                  <li>
                      <a href="#"><img src="assets/img/footer/card1.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card2.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card3.jpg" alt="card"></a>
                  </li>
                  <li>
                      <a href="#"><img src="assets/img/footer/card4.jpg" alt="card"></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Copyright End -->

    </footer>
    <!-- Footer Section End --> 

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
      <i class="lni-chevron-up"></i>
    </a>

    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->

     <!-- jQuery first, then Popper.js, then Bootstrap JS -->
     <script src="/assets/js/jquery-min.js"></script>
     <script src="/assets/js/popper.min.js"></script>
     <script src="/assets/js/bootstrap.min.js"></script>
     <script src="/assets/js/jquery.counterup.min.js"></script>
     <script src="/assets/js/waypoints.min.js"></script>
     <script src="/assets/js/wow.js"></script>
     <script src="/assets/js/owl.carousel.min.js"></script>
     <script src="/assets/js/nivo-lightbox.js"></script>
     <script src="/assets/js/jquery.slicknav.js"></script>
     <script src="/assets/js/main.js"></script>
     <script src="/assets/js/form-validator.min.js"></script>
     <script src="/assets/js/contact-form-script.min.js"></script>
     <script src="/assets/js/summernote.js"></script>
      
  </body>
</html>