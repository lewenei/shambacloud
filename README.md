# ![Shambacloud]

> ### Classified Ad Posting App for farmers.
Link Farmers to Vet Services




----------

# Getting started

## Installation


Clone the repository

    git clone 

Switch to the repo folder

    cd marketplace

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000




## Database seeding
Open the RolesTableDataSeeder,UsersTableDataSeeder and set the property values as per your requirement

    database/seeds/RolesTableDataSeeder.php
    database/seeds/UsersTableDataSeeder.php

Run the database seeder and you're done

    php artisan db:seed





## New Stuff

Registration Page
Messaging (chatify)
OTP
Restricting Access to Phone number until payment/subscription
