<?php

namespace App\Providers;

use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $all_categories = Category::with('addposts')->get();

        $max_price = DB::table('add_posts')->max('price');
        $min_price = DB::table('add_posts')->min('price');


        $all_division =  array(
            'Baringo'=> 'Baringo',
    'Bomet'=> 'Bomet',
    'Bungoma'=> 'Bungoma',
    'Busia'=> 'Busia',
    'Elgeyo-Marakwet'=> 'Elgeyo-Marakwet',
    'Embu'=> 'Embu',
    'Garissa'=> 'Garissa',
    'Homa Bay'=> 'Homa Bay',
    'Isiolo'=> 'Isiolo',
    'Kajiado'=> 'Kajiado',
    'Kakamega'=> 'Kakamega',
    'Kericho'=> 'Kericho',
    'Kiambu'=> 'Kiambu',
    'Kilifi'=> 'Kilifi',
    'Kirinyaga'=> 'Kirinyaga',
    'Kisii'=> 'Kisii',
    'Kisumu'=> 'Kisumu',
    'Kitui'=> 'Kitui',
    'Kwale'=> 'Kwale',
    'Laikipia'=> 'Laikipia',
    'Lamu'=> 'Lamu',
    'Machakos'=> 'Machakos',
    'Makueni'=> 'Makueni',
    'Mandera'=> 'Mandera',
    'Marsabit'=> 'Marsabit',
    'Meru'=> 'Meru',
    'Migori'=> 'Migori',
    'Mombasa'=> 'Mombasa',
    'Muranga'=> 'Muranga',
    'Nairobi City'=> 'Nairobi City',
    'Nakuru'=> 'Nakuru',
    'Nandi'=> 'Nandi',
    'Narok'=> 'Narok',
    'Nyamira'=> 'Nyamira',
    'Nyandarua'=> 'Nyandarua',
    'Nyeri'=> 'Nyeri',
    'Samburu'=> 'Samburu',
    'Siaya'=> 'Siaya',
    'Taita-Taveta'=> 'Taita-Taveta',
    'Tana River'=> 'Tana River',
    'Tharaka-Nithi'=> 'Tharaka-Nithi',
    'Trans Nzoia'=> 'Trans Nzoia',
    'Turkana'=> 'Turkana',
    'Uasin Gishu'=> 'Uasin Gishu',
    'Vihiga'=> 'Vihiga',
    'West Pokot'=> 'West Pokot',
    'wajir'=> 'wajir',
        );

        $add_type =  array(
            'I want to buy' => 'I want to buy',
            'I Want To Sell' => 'I Want To Sell',
        );

        $condition_type =  array(
            'New' => 'New',
            'Used' => 'Used',
            'Fresh' => 'Fresh',
            'Dried' => 'Dried',

        );

        View::share([
            'all_categories' => $all_categories,
            'all_division' => $all_division,
            'add_type' => $add_type,
            'condition_type' => $condition_type,
            'max_price' => $max_price,
            'min_price' => $min_price
        ]);

    }
}
